import { expect } from "chai";
import { BigNumber } from "ethers";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { encodePrice } from "./shared/utilities";
import {
  ContractRegistry,
  ERC20Core,
  UniswapV2Factory,
  UniswapV2Pair,
} from "../../typechain";

const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);
const REVENUE_POOL_KEY = "governance.intApp.QDEX.revenuePool";
const ROUTER_KEY = "governance.intApp.QDEX.router";

const overrides = {
  gasLimit: 9999999,
};

const mineBlock = async (secondsInFuture: number) => {
  const latestBlock = await ethers.provider.getBlock("latest");
  await ethers.provider.send("evm_mine", [
    latestBlock.timestamp + secondsInFuture,
  ]);
};

const mineBlockExactTime = async (seconds: number) => {
  await ethers.provider.send("evm_mine", [seconds]);
};

describe("UniswapV2Pair", () => {
  let wallet: SignerWithAddress, other: SignerWithAddress;

  let factory: UniswapV2Factory;
  let token0: ERC20Core;
  let token1: ERC20Core;
  let pair: UniswapV2Pair;

  let registry: ContractRegistry;
  beforeEach(async () => {
    [wallet, other] = await ethers.getSigners();

    const registryFactory = await ethers.getContractFactory("ContractRegistry");

    registry = await registryFactory.deploy();
    await registry.initialize(
      [wallet.address],
      [REVENUE_POOL_KEY],
      [wallet.address]
    ); // Let the wallet earn the 0.05% fee

    const Factory = await ethers.getContractFactory("UniswapV2Factory");
    factory = await Factory.deploy(registry.address);

    const tokenFactory = await ethers.getContractFactory("ERC20Core");
    const tokenA = await tokenFactory.deploy(ethers.utils.parseEther("10000"));
    const tokenB = await tokenFactory.deploy(ethers.utils.parseEther("10000"));

    await factory.createPair(tokenA.address, tokenB.address);
    const pairAddress = await factory.getPair(tokenA.address, tokenB.address);

    pair = await ethers.getContractAt("UniswapV2Pair", pairAddress);
    const token0Address = await pair.token0();

    token0 = tokenA.address === token0Address ? tokenA : tokenB;
    token1 = tokenA.address === token0Address ? tokenB : tokenA;

    await registry.setAddress(ROUTER_KEY, wallet.address); // Bypass onlyRouter for testing purposes
  });

  it("mint", async () => {
    const token0Amount = ethers.utils.parseEther("1");
    const token1Amount = ethers.utils.parseEther("4");
    await token0.transfer(pair.address, token0Amount);
    await token1.transfer(pair.address, token1Amount);

    const expectedLiquidity = ethers.utils.parseEther("2");
    await expect(pair.mint(wallet.address, overrides))
      .to.emit(pair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        ethers.constants.AddressZero,
        MINIMUM_LIQUIDITY
      )
      .to.emit(pair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        wallet.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(pair, "Sync")
      .withArgs(token0Amount, token1Amount)
      .to.emit(pair, "Mint")
      .withArgs(wallet.address, token0Amount, token1Amount);

    expect(await pair.totalSupply()).to.eq(expectedLiquidity);
    expect(await pair.balanceOf(wallet.address)).to.eq(
      expectedLiquidity.sub(MINIMUM_LIQUIDITY)
    );
    expect(await token0.balanceOf(pair.address)).to.eq(token0Amount);
    expect(await token1.balanceOf(pair.address)).to.eq(token1Amount);
    const reserves = await pair.getReserves();
    expect(reserves[0]).to.eq(token0Amount);
    expect(reserves[1]).to.eq(token1Amount);
  });

  async function addLiquidity(
    token0Amount: BigNumber,
    token1Amount: BigNumber
  ) {
    await token0.transfer(pair.address, token0Amount);
    await token1.transfer(pair.address, token1Amount);
    await pair.mint(wallet.address, overrides);
  }
  const swapTestCases: BigNumber[][] = [
    [1, 5, 10, "1662497915624478906"],
    [1, 10, 5, "453305446940074565"],

    [2, 5, 10, "2851015155847869602"],
    [2, 10, 5, "831248957812239453"],

    [1, 10, 10, "906610893880149131"],
    [1, 100, 100, "987158034397061298"],
    [1, 1000, 1000, "996006981039903216"],
  ].map((a) =>
    a.map((n) =>
      typeof n === "string"
        ? BigNumber.from(n)
        : ethers.utils.parseEther(n.toString())
    )
  );
  swapTestCases.forEach((swapTestCase, i) => {
    it(`getInputPrice:${i}`, async () => {
      const [swapAmount, token0Amount, token1Amount, expectedOutputAmount] =
        swapTestCase;
      await addLiquidity(token0Amount, token1Amount);
      await token0.transfer(pair.address, swapAmount);
      await expect(
        pair.swap(
          0,
          expectedOutputAmount.add(1),
          wallet.address,
          "0x",
          overrides
        )
      ).to.be.revertedWith("UniswapV2: K");
      await pair.swap(0, expectedOutputAmount, wallet.address, "0x", overrides);
    });
  });

  const optimisticTestCases: BigNumber[][] = [
    ["997000000000000000", 5, 10, 1], // given amountIn, amountOut = floor(amountIn * .997)
    ["997000000000000000", 10, 5, 1],
    ["997000000000000000", 5, 5, 1],
    [1, 5, 5, "1003009027081243732"], // given amountOut, amountIn = ceiling(amountOut / .997)
  ].map((a) =>
    a.map((n) =>
      typeof n === "string"
        ? BigNumber.from(n)
        : ethers.utils.parseEther(n.toString())
    )
  );
  optimisticTestCases.forEach((optimisticTestCase, i) => {
    it(`optimistic:${i}`, async () => {
      const [outputAmount, token0Amount, token1Amount, inputAmount] =
        optimisticTestCase;
      await addLiquidity(token0Amount, token1Amount);
      await token0.transfer(pair.address, inputAmount);
      await expect(
        pair.swap(outputAmount.add(1), 0, wallet.address, "0x", overrides)
      ).to.be.revertedWith("UniswapV2: K");
      await pair.swap(outputAmount, 0, wallet.address, "0x", overrides);
    });
  });

  it("swap:token0", async () => {
    const token0Amount = ethers.utils.parseEther("5");
    const token1Amount = ethers.utils.parseEther("10");
    await addLiquidity(token0Amount, token1Amount);

    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("1662497915624478906");
    await token0.transfer(pair.address, swapAmount);
    await expect(
      pair.swap(0, expectedOutputAmount, wallet.address, "0x", overrides)
    )
      .to.emit(token1, "Transfer")
      .withArgs(pair.address, wallet.address, expectedOutputAmount)
      .to.emit(pair, "Sync")
      .withArgs(
        token0Amount.add(swapAmount),
        token1Amount.sub(expectedOutputAmount)
      )
      .to.emit(pair, "Swap")
      .withArgs(
        wallet.address,
        swapAmount,
        0,
        0,
        expectedOutputAmount,
        wallet.address
      );

    const reserves = await pair.getReserves();
    expect(reserves[0]).to.eq(token0Amount.add(swapAmount));
    expect(reserves[1]).to.eq(token1Amount.sub(expectedOutputAmount));
    expect(await token0.balanceOf(pair.address)).to.eq(
      token0Amount.add(swapAmount)
    );
    expect(await token1.balanceOf(pair.address)).to.eq(
      token1Amount.sub(expectedOutputAmount)
    );
    const totalSupplyToken0 = await token0.totalSupply();
    const totalSupplyToken1 = await token1.totalSupply();
    expect(await token0.balanceOf(wallet.address)).to.eq(
      totalSupplyToken0.sub(token0Amount).sub(swapAmount)
    );
    expect(await token1.balanceOf(wallet.address)).to.eq(
      totalSupplyToken1.sub(token1Amount).add(expectedOutputAmount)
    );
  });

  it("swap:token1", async () => {
    const token0Amount = ethers.utils.parseEther("5");
    const token1Amount = ethers.utils.parseEther("10");
    await addLiquidity(token0Amount, token1Amount);

    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("453305446940074565");
    await token1.transfer(pair.address, swapAmount);
    await expect(
      pair.swap(expectedOutputAmount, 0, wallet.address, "0x", overrides)
    )
      .to.emit(token0, "Transfer")
      .withArgs(pair.address, wallet.address, expectedOutputAmount)
      .to.emit(pair, "Sync")
      .withArgs(
        token0Amount.sub(expectedOutputAmount),
        token1Amount.add(swapAmount)
      )
      .to.emit(pair, "Swap")
      .withArgs(
        wallet.address,
        0,
        swapAmount,
        expectedOutputAmount,
        0,
        wallet.address
      );

    const reserves = await pair.getReserves();
    expect(reserves[0]).to.eq(token0Amount.sub(expectedOutputAmount));
    expect(reserves[1]).to.eq(token1Amount.add(swapAmount));
    expect(await token0.balanceOf(pair.address)).to.eq(
      token0Amount.sub(expectedOutputAmount)
    );
    expect(await token1.balanceOf(pair.address)).to.eq(
      token1Amount.add(swapAmount)
    );
    const totalSupplyToken0 = await token0.totalSupply();
    const totalSupplyToken1 = await token1.totalSupply();
    expect(await token0.balanceOf(wallet.address)).to.eq(
      totalSupplyToken0.sub(token0Amount).add(expectedOutputAmount)
    );
    expect(await token1.balanceOf(wallet.address)).to.eq(
      totalSupplyToken1.sub(token1Amount).sub(swapAmount)
    );
  });

  it("swap:gas", async () => {
    const token0Amount = ethers.utils.parseEther("5");
    const token1Amount = ethers.utils.parseEther("10");
    await addLiquidity(token0Amount, token1Amount);

    // ensure that setting price{0,1}CumulativeLast for the first time doesn't affect our gas math
    await mineBlock(1);

    await pair.sync(overrides);

    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("453305446940074565");
    await token1.transfer(pair.address, swapAmount);
    await mineBlock(1);
    const tx = await pair.swap(
      expectedOutputAmount,
      0,
      wallet.address,
      "0x",
      overrides
    );
    const receipt = await tx.wait();
    expect(receipt.gasUsed).to.eq(82285);
  });

  it("burn", async () => {
    const token0Amount = ethers.utils.parseEther("3");
    const token1Amount = ethers.utils.parseEther("3");
    await addLiquidity(token0Amount, token1Amount);

    const expectedLiquidity = ethers.utils.parseEther("3");
    await pair.transfer(pair.address, expectedLiquidity.sub(MINIMUM_LIQUIDITY));
    await expect(pair.burn(wallet.address, overrides))
      .to.emit(pair, "Transfer")
      .withArgs(
        pair.address,
        ethers.constants.AddressZero,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(token0, "Transfer")
      .withArgs(pair.address, wallet.address, token0Amount.sub(1000))
      .to.emit(token1, "Transfer")
      .withArgs(pair.address, wallet.address, token1Amount.sub(1000))
      .to.emit(pair, "Sync")
      .withArgs(1000, 1000)
      .to.emit(pair, "Burn")
      .withArgs(
        wallet.address,
        token0Amount.sub(1000),
        token1Amount.sub(1000),
        wallet.address
      );

    expect(await pair.balanceOf(wallet.address)).to.eq(0);
    expect(await pair.totalSupply()).to.eq(MINIMUM_LIQUIDITY);
    expect(await token0.balanceOf(pair.address)).to.eq(1000);
    expect(await token1.balanceOf(pair.address)).to.eq(1000);
    const totalSupplyToken0 = await token0.totalSupply();
    const totalSupplyToken1 = await token1.totalSupply();
    expect(await token0.balanceOf(wallet.address)).to.eq(
      totalSupplyToken0.sub(1000)
    );
    expect(await token1.balanceOf(wallet.address)).to.eq(
      totalSupplyToken1.sub(1000)
    );
  });

  it("price{0,1}CumulativeLast", async () => {
    const token0Amount = ethers.utils.parseEther("3");
    const token1Amount = ethers.utils.parseEther("3");
    await addLiquidity(token0Amount, token1Amount);

    const blockTimestamp = (await pair.getReserves())[2];
    await pair.sync(overrides);

    const initialPrice = encodePrice(token0Amount, token1Amount);
    expect(await pair.price0CumulativeLast()).to.eq(initialPrice[0]);
    expect(await pair.price1CumulativeLast()).to.eq(initialPrice[1]);
    expect((await pair.getReserves())[2]).to.eq(blockTimestamp + 1);

    const swapAmount = ethers.utils.parseEther("3");
    await token0.transfer(pair.address, swapAmount);
    await mineBlock(7);
    // swap to a new price eagerly instead of syncing
    await pair.swap(
      0,
      ethers.utils.parseEther("1"),
      wallet.address,
      "0x",
      overrides
    ); // make the price nice

    expect(await pair.price0CumulativeLast()).to.eq(initialPrice[0].mul(10));
    expect(await pair.price1CumulativeLast()).to.eq(initialPrice[1].mul(10));
    expect((await pair.getReserves())[2]).to.eq(blockTimestamp + 10);

    await mineBlockExactTime(blockTimestamp + 19);
    await pair.sync(overrides);

    const newPrice = encodePrice(
      ethers.utils.parseEther("6"),
      ethers.utils.parseEther("2")
    );
    expect(await pair.price0CumulativeLast()).to.eq(
      initialPrice[0].mul(10).add(newPrice[0].mul(10))
    );
    expect(await pair.price1CumulativeLast()).to.eq(
      initialPrice[1].mul(10).add(newPrice[1].mul(10))
    );
    expect((await pair.getReserves())[2]).to.eq(blockTimestamp + 20);
  });

  /** The fee can't be toggled off at the moment
  it("feeTo:off", async () => {
    const token0Amount = ethers.utils.parseEther("1000");
    const token1Amount = ethers.utils.parseEther("1000");
    await addLiquidity(token0Amount, token1Amount);

    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("996006981039903216");
    await token1.transfer(pair.address, swapAmount);
    await pair.swap(expectedOutputAmount, 0, wallet.address, "0x", overrides);

    const expectedLiquidity = ethers.utils.parseEther("1000");
    await pair.transfer(pair.address, expectedLiquidity.sub(MINIMUM_LIQUIDITY));
    await pair.burn(wallet.address, overrides);
    expect(await pair.totalSupply()).to.eq(MINIMUM_LIQUIDITY);
  });
 */

  it("feeTo:on", async () => {
    //await factory.setFeeTo(other.address);

    const token0Amount = ethers.utils.parseEther("1000");
    const token1Amount = ethers.utils.parseEther("1000");
    await addLiquidity(token0Amount, token1Amount);

    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("996006981039903216");
    await token1.transfer(pair.address, swapAmount);
    await pair.swap(expectedOutputAmount, 0, wallet.address, "0x", overrides);

    const expectedLiquidity = ethers.utils.parseEther("1000");
    await pair.transfer(pair.address, expectedLiquidity.sub(MINIMUM_LIQUIDITY));
    await pair.burn(wallet.address, overrides);
    expect(await pair.totalSupply()).to.eq(
      MINIMUM_LIQUIDITY.add("249750499251388")
    );

    const revenuePoolAddress = await registry.mustGetAddress(REVENUE_POOL_KEY);
    expect(await pair.balanceOf(revenuePoolAddress)).to.eq("249750499251388");

    // using 1000 here instead of the symbolic MINIMUM_LIQUIDITY because the amounts only happen to be equal...
    // ...because the initial liquidity amounts were equal
    expect(await token0.balanceOf(pair.address)).to.eq(
      BigNumber.from(1000).add("249501683697445")
    );
    expect(await token1.balanceOf(pair.address)).to.eq(
      BigNumber.from(1000).add("250000187312969")
    );
  });
});

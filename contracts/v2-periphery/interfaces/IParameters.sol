// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity >=0.5.0;
pragma experimental ABIEncoderV2;

interface IParameters {
    function getAddrKeys() external view returns (string[] memory);

    function getUintKeys() external view returns (string[] memory);

    function getStringKeys() external view returns (string[] memory);

    function getBytes32Keys() external view returns (string[] memory);

    function getBoolKeys() external view returns (string[] memory);

    function getAddr(string calldata _key) external view returns (address);

    function getUint(string calldata _key) external view returns (uint256);

    function getString(string calldata _key)
        external
        view
        returns (string memory);

    function getBytes32(string calldata _key) external view returns (bytes32);

    function getBool(string calldata _key) external view returns (bool);
}
